// @ts-ignore
export const load = async ({ fetch }) => {
	const getPosts = async () => {
		// const res = await fetch('https://jsonplaceholder.typicode.com/photos');
		const res = await fetch('/api/posts.json');
		const data = await res.json();
		return data;
		// const filteredData = data.slice(0, 3);
		// return filteredData;
	};

	return {
		posts: getPosts()
	};
};
