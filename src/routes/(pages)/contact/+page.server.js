import { object, string } from 'yup';

export const actions = {
	// @ts-ignore
	default: async ({ request }) => {
		const formData = await request.formData();
		const name = formData.get('name');
		const email = formData.get('email');
		const message = formData.get('message');

		let contactFormSchema = object({
			name: string().min(2).required(),
			email: string().required().email(),
			message: string().required()
		});

		try {
			await contactFormSchema.validate(
				{
					name,
					email,
					message
				},
				{ abortEarly: false }
			);
			return {
				success: true,
				status: 'Form is submitted.'
			};
		} catch (error) {
			// @ts-ignore
			const errors = error.inner.reduce((acc, err) => {
				return { ...acc, [err.path]: err.message };
			}, {});

			return { errors, name, email, message };
		}
	}
};
