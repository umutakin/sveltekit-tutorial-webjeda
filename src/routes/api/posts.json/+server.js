import { json } from '@sveltejs/kit';

export const GET = async () => {
	const posts = [
		{
			id: 1,
			image: 'https://picsum.photos/id/10/800/500',
			title: 'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
			body: 'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto'
		},
		{
			id: 2,
			image: 'https://picsum.photos/id/17/800/500',
			title: 'qui est esse',
			body: 'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla'
		},
		{
			id: 3,
			image: 'https://picsum.photos/id/49/800/500',
			title: 'ea molestias quasi exercitationem repellat qui ipsa sit aut',
			body: 'et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut'
		}
	];

	return json(posts);

	// return new Response(JSON.stringify(posts), {
	// 	headers: {
	// 		'Content-Type': 'application/json'
	// 	}
	// });
};
